package com.desarmaduria.springboot.app.rest;



import com.desarmaduria.springboot.app.service.ClienteService;

import com.fasterxml.jackson.databind.ObjectMapper;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.desarmaduria.springboot.app.model.Cliente;




@ExtendWith(MockitoExtension.class)
class ClienteRestControllerTest {
	
	private MockMvc mockMvc;
	
	@Mock
	private ClienteService clienteService;
	
	@InjectMocks
	private ClienteRestController clienteRestController;
	
	@BeforeEach
	void setup() {
		JacksonTester.initFields(this, new ObjectMapper());
		mockMvc = MockMvcBuilders.standaloneSetup(clienteRestController).build();
	}

	@Test
	void siSeInvocaObtenerClienteByIdYHallaElClienteDevuelveElEstadoOK() throws Exception {
	    Cliente cliente;
		// Given
		cliente = new Cliente(200775430, "Giovann", "Romero");
		
		given(clienteService.findClienteById(1)).willReturn(cliente);
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/clientes/1")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

	}
	@Test
	void siSeInvocaGetClienteByIdYNoloEncuentraDevuelveNOTFOUND() throws Exception {
		//Given
		 Cliente cliente;
		 
		cliente = null;
		
		given(clienteService.findClienteById(1)).willReturn(cliente);
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/clientes/1")
				
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}
	@Test
	void siSeInvocaAñadirClienteYSeAñadeConExitoRetornaElEstadoCreated() throws Exception {
	
		
		Cliente cliente = new Cliente(20077543,"Alexandra","Romero");
    	cliente.setId(999);
    	ObjectMapper mapper = new ObjectMapper();
    	String clienteJSON = mapper.writeValueAsString(cliente);
    	this.mockMvc.perform(post("/clientes")
    		.content(clienteJSON).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
    		.andExpect(status().isCreated());
		
	}
	@Test
	void siSeInvocaAñadirClienteYNoSeEnviaUnClienteValidoRetornaBadRequest() throws Exception {
	
		Cliente cliente =null;
    
    	ObjectMapper mapeo= new ObjectMapper();
    	String clienteJSON = mapeo.writeValueAsString(cliente);
    	this.mockMvc.perform(post("/clientes")
        .content(clienteJSON).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest()).andDo(MockMvcResultHandlers.print());
		
	}
	
	
	
	
}
