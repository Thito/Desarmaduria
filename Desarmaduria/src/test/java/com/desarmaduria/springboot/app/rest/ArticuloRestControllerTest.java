package com.desarmaduria.springboot.app.rest;

import com.fasterxml.jackson.databind.ObjectMapper;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.desarmaduria.springboot.app.model.Articulo;
import com.desarmaduria.springboot.app.service.ArticuloService;

@ExtendWith(MockitoExtension.class)
class ArticuloRestControllerTest {
	
	private MockMvc mockMvc;
	
	@Mock
	private ArticuloService articuloService;
	
	@InjectMocks
	private ArticuloRestController articuloRestController;
	
	@BeforeEach
	void setup() {
		JacksonTester.initFields(this, new ObjectMapper());
		mockMvc = MockMvcBuilders.standaloneSetup(articuloRestController).build();
	}

	@Test
	void siSeInvocagetUbicacionArticuloByIdYHallaLaUbicacionDevuelveElEstadoOK() throws Exception {
	    Articulo articulo;
		// Given
		articulo = new Articulo("Manubrio","bodega1",25000);
		
		given(articuloService.findArticuloById(1)).willReturn(articulo);
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/ubicacion/1")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

	}
	
	@Test
	void siSeInvocagetUbicacionArticuloByIdYNoSeEncuentraLaUbicacionDevuelveElEstadoNotFound() throws Exception {
	    Articulo articulo;
	    
	    articulo=null;
		// Given
		
		given(articuloService.findArticuloById(1)).willReturn(articulo);
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/ubicacion/1")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());

	}
	
	@Test
	void siSeInvocagetStockArticuloByNombreYSeHallaElArticuloDevuelveElEstadoOk() throws Exception {
	    
		// Given
		
		given(articuloService.getStockByNombreArticulo("Rueda")).willReturn(1);
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/stock/Rueda")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

	}
	@Test
	void siSeInvocagetStockArticuloByNombreYNoSeHallaElArticuloDevuelveElEstadoNotFound() throws Exception {
	    Articulo articulo;
	    
	    articulo=new Articulo("Rueda","bodega1",15000);
	    articuloService.save(articulo);
		// Given
		
	    given(articuloService.getStockByNombreArticulo("Manubrio")).willReturn(0);
		
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/stock/Manubrio")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());

	}
	
	@Test
	void siSeInvocagetArticulosByNombreBodegaYSeEncuentranArticulosRetornaOk() throws Exception {
	    Articulo articulo;
	    
	    List<Articulo>articulos=new ArrayList<Articulo>();
	    
	    articulo=new Articulo("Rueda","bodega1",15000);
	    articuloService.save(articulo);
	    articulos.add(articulo);
	    
	    
	  
	    
		// Given
		
	    given(articuloService.getArticulosPorBodega("bodega1")).willReturn(articulos);
		
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/articulosEnBodega/bodega1")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());

	}
	
	@Test
	void siSeInvocagetArticulosByNombreBodegaYNoSeEncuentranArticulosRetornaNOT_FOUND() throws Exception {
	    
	    List<Articulo>articulos=new ArrayList<Articulo>();
	    
		// Given
		
	    given(articuloService.getArticulosPorBodega("bodega1")).willReturn(articulos);
		
		
		// When
		MockHttpServletResponse response = mockMvc.perform(get("/articulos/articulosEnBodega/bodega1")
				.accept(MediaType.APPLICATION_JSON))
				
				.andReturn()
				
				.getResponse();
		// Then
		assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());

	}
	
	
}
