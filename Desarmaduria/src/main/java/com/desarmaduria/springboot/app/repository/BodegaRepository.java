package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Bodega;

public interface BodegaRepository {

	List<Bodega> findAll();

	Bodega findById(int id);

	void save(Bodega bodega);

	void deleteById(int id);

}
