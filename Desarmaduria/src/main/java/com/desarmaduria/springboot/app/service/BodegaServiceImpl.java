package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Bodega;
import com.desarmaduria.springboot.app.repository.BodegaRepository;

@Service
public class BodegaServiceImpl implements BodegaService {

	@Autowired
	private BodegaRepository bodegaRepository;

	@Override
	public List<Bodega> findAllBodegas() {
		return bodegaRepository.findAll();
	}

	@Override
	public Bodega findBodegaById(int idBodega) throws DesarmaduriaException{
		Bodega bodega = bodegaRepository.findById(idBodega);
		if (bodega != null) {
			return bodega;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Bodega bodega) throws DesarmaduriaException{
		if (bodega == null) {
			throw new DesarmaduriaException();
		}
		try {
			bodegaRepository.save(bodega);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteBodegaById(int idBodega) throws DesarmaduriaException {
		try {
			bodegaRepository.deleteById(idBodega);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
