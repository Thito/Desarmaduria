package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Venta;

public interface VentaRepository {

	List<Venta> findAll();

	Venta findById(int id);

	void save(Venta venta);

	void deleteById(int id);

}
