package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Mecanico;

public interface MecanicoRepository {

	List<Mecanico> findAll();

	Mecanico findById(int id);

	void save(Mecanico mecanico);

	void deleteById(int id);

}
