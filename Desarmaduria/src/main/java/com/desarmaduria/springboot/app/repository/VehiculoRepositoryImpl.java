package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Vehiculo;

@Repository
@Profile("jpa")
public class VehiculoRepositoryImpl implements VehiculoRepository {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Vehiculo> findAll() {
		return this.em.createQuery("SELECT vehiculo FROM Vehiculo vehiculo").getResultList();
	}
	
	@Override
	public Vehiculo findById(int id) {
		return this.em.find(Vehiculo.class, id);
	}
	
	@Override
	public void save(Vehiculo vehiculo)  {
        if (vehiculo.getId() == null) {
            this.em.persist(vehiculo);
        } else {
            this.em.merge(vehiculo);
        }
	}

	@Override
	public void deleteById(int id) {
		Vehiculo vehiculo = this.findById(id);
		this.em.remove(vehiculo);
	}
}
