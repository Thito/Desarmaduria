package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Mecanico;

@Repository
@Profile("jpa")
public class MecanicoRepositoryImpl implements MecanicoRepository {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Mecanico> findAll() {
		return this.em.createQuery("SELECT mecanico FROM Mecanico mecanico").getResultList();
	}
	
	@Override
	public Mecanico findById(int id) {
		return this.em.find(Mecanico.class, id);
	}
	
	@Override
	public void save(Mecanico mecanico)  {
        if (mecanico.getId() == null) {
            this.em.persist(mecanico);
        } else {
            this.em.merge(mecanico);
        }
	}

	@Override
	public void deleteById(int id) {
		Mecanico mecanico = this.findById(id);
		this.em.remove(mecanico);
	}
}
