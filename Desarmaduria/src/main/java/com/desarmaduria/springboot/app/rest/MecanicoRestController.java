package com.desarmaduria.springboot.app.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Mecanico;
import com.desarmaduria.springboot.app.model.Vehiculo;
import com.desarmaduria.springboot.app.model.Vendedor;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.MecanicoService;
import com.desarmaduria.springboot.app.service.VehiculoService;
import com.desarmaduria.springboot.app.service.VendedorService;

@RestController
@RequestMapping("mecanicos")
public class MecanicoRestController {
	
	@Autowired
	private MecanicoService mecanicoService;
	
	@Autowired
	private VendedorService vendedorService; 
	
	@Autowired
	private VehiculoService vehiculoService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Mecanico>> getAllMecanicos() {
		List<Mecanico> lista = mecanicoService.findAllMecanicos();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Mecanico>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Mecanico>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idMecanico}", produces="application/json")
	public ResponseEntity<Mecanico> getMecanicoById(@PathVariable("idMecanico") int id) throws DesarmaduriaException {
		Mecanico mecanico = mecanicoService.findMecanicoById(id);
		if (mecanico == null) {
			return new ResponseEntity<Mecanico>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Mecanico>(mecanico, HttpStatus.OK);
		}
	}
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Mecanico> addMecanico(@Validated @RequestBody Mecanico mecanico) {
		try {
			Vendedor vendedor=vendedorService.findVendedorById(mecanico.getVendedor().getId());
			
			
			
		ArrayList<Vehiculo> vehiculos=(ArrayList<Vehiculo>) vehiculoService.findAllVehiculo();
			
			ArrayList<Vehiculo>nuevo=new ArrayList<Vehiculo>();
			
			for(int i=0;i<mecanico.getVehiculos().size();i++) {
				for(int j=0;j<vehiculos.size();j++) {
					if(mecanico.getVehiculos().get(i).getId()==vehiculos.get(j).getId()) {
						nuevo.add(vehiculos.get(j));
					}
				}
			}
			
			
			mecanico.setVendedor(vendedor);
			
			mecanico.setVehiculos(vehiculos);
			mecanicoService.save(mecanico);
			
			
			return new ResponseEntity<Mecanico>(mecanico, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Mecanico>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idMecanico}")
	public ResponseEntity<Mecanico> updateMecanico(@PathVariable("idMecanico") int idMecanico, @RequestBody Mecanico mecanico) throws DesarmaduriaException{
			
		Mecanico mecanico1 = mecanicoService.findMecanicoById(idMecanico);
			if (mecanico1 == null) {
				return new ResponseEntity<Mecanico>(HttpStatus.NOT_FOUND);
			}
			
			Vendedor vendedor=vendedorService.findVendedorById(mecanico.getVendedor().getId());
			
			ArrayList<Vehiculo> vehiculos=(ArrayList<Vehiculo>) vehiculoService.findAllVehiculo();
			
			ArrayList<Vehiculo>nuevo=new ArrayList<Vehiculo>();
			
			for(int i=0;i<mecanico.getVehiculos().size();i++) {
				for(int j=0;j<vehiculos.size();j++) {
					if(mecanico.getVehiculos().get(i).getId()==vehiculos.get(j).getId()) {
						nuevo.add(vehiculos.get(j));
					}
				}
			}
			
			
			
			mecanico1.setNombre(mecanico.getNombre());
			mecanico1.setApellido(mecanico.getApellido());
			mecanico1.setTelefono(mecanico.getTelefono());
			mecanico1.setVendedor(vendedor);
			mecanico1.setVehiculos(nuevo);
			
			mecanicoService.save(mecanico1);
			
			return new ResponseEntity<Mecanico>(mecanico1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idMecanico}", produces="application/json")
	public ResponseEntity<Mecanico> deleteMecanicoById(@PathVariable("idMecanico") int idMecanico) {
		try {
			mecanicoService.deleteMecanicoById(idMecanico);
			return new ResponseEntity<Mecanico>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Mecanico>(HttpStatus.NOT_FOUND);
		}
	}	
	
}
