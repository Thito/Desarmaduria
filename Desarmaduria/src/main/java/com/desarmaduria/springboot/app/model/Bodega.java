package com.desarmaduria.springboot.app.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="bodega")
public class Bodega {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nombre;
	
	@JoinColumn(name="capacidad_De_Almacenamiento")
	private int capacidadDeAlmacenamiento;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Articulo>articulos;
	
	public Bodega() {}
	
	public Bodega(String nombre, int capacidadDeAlmacenamiento ) {
		this.capacidadDeAlmacenamiento = capacidadDeAlmacenamiento;
		this.articulos = new ArrayList<Articulo>();
		this.nombre=nombre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCapacidadDeAlmacenamiento() {
		return capacidadDeAlmacenamiento;
	}

	public void setCapacidadDeAlmacenamiento(int capacidadDeAlmacenamiento) {
		this.capacidadDeAlmacenamiento = capacidadDeAlmacenamiento;
	}

	public List<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	
	
	
	
}
