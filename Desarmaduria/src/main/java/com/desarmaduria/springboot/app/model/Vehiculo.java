package com.desarmaduria.springboot.app.model;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="vehiculo")
public class Vehiculo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String marca;
	private String modelo;
	private int anio;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id_Cliente3")
	private Cliente cliente;
	
	@ManyToMany(mappedBy="vehiculos")
	@JsonIgnoreProperties(value="vehiculos")
    List<Mecanico> mecanicos;
	
	@ManyToOne
	@JoinColumn(name = "id_Garage")
	private Garage garage;
	
	public Vehiculo() {}

	public Vehiculo(String marca, String modelo, int año, Cliente cliente, List<Mecanico> mecanicos, Garage garage) {
		this.marca = marca;
		this.modelo = modelo;
		this.anio = año;
		this.cliente = cliente;
		this.mecanicos = mecanicos;
		this.garage = garage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getAño() {
		return anio;
	}

	public void setAño(int año) {
		this.anio = año;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Mecanico> getMecanicos() {
		if(mecanicos==null) {
			return new ArrayList<Mecanico>();
		}
		return mecanicos;
	}

	public void setMecanicos(List<Mecanico> mecanicos) {
		this.mecanicos = mecanicos;
	}

	public Garage getGarage() {
		return garage;
	}

	public void setGarage(Garage garage) {
		this.garage = garage;
	}
	
	
	

}
