package com.desarmaduria.springboot.app.service;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Mecanico;
import com.desarmaduria.springboot.app.repository.MecanicoRepository;
@Service
public class MecanicoServiceImpl implements MecanicoService {

	@Autowired
	private MecanicoRepository mecanicoRepository;

	@Override
	public List<Mecanico> findAllMecanicos() {
		return mecanicoRepository.findAll();
	}

	@Override
	public Mecanico findMecanicoById(int idMecanico) throws DesarmaduriaException{
		Mecanico mecanico = mecanicoRepository.findById(idMecanico);
		if (mecanico != null) {
			return mecanico;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Mecanico mecanico) throws DesarmaduriaException{
		if (mecanico == null) {
			throw new DesarmaduriaException();
		}
		try {
			mecanicoRepository.save(mecanico);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteMecanicoById(int idMecanico) throws DesarmaduriaException {
		try {
			mecanicoRepository.deleteById(idMecanico);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
