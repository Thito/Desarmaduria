package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Revision;

public interface RevisionService {

	List<Revision> findAllRevisiones();

	Revision findRevisionById(int idRevision) throws DesarmaduriaException;

	void save(Revision revision) throws DesarmaduriaException;

	void deleteRevisionById(int idRevision) throws DesarmaduriaException;

}
