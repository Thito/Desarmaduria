package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Garage;

public interface GarageService {

	List<Garage> findAllGarages();

	Garage findGarageById(int idGarage) throws DesarmaduriaException;

	void save(Garage garage) throws DesarmaduriaException;

	void deleteGarageById(int idGarage) throws DesarmaduriaException;

}
