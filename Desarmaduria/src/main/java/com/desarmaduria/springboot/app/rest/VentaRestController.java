package com.desarmaduria.springboot.app.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Articulo;
import com.desarmaduria.springboot.app.model.Cliente;
import com.desarmaduria.springboot.app.model.Vendedor;
import com.desarmaduria.springboot.app.model.Venta;
import com.desarmaduria.springboot.app.service.ArticuloService;
import com.desarmaduria.springboot.app.service.ClienteService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.VendedorService;
import com.desarmaduria.springboot.app.service.VentaService;

@RestController
@RequestMapping("ventas")
public class VentaRestController {

	@Autowired
	private VentaService ventaService;
	@Autowired
	private VendedorService vendedorService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private ArticuloService articuloService;
	
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Venta>> getAllVentas() {
		List<Venta> lista = ventaService.findAllVentas();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Venta>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Venta>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idVenta}", produces="application/json")
	public ResponseEntity<Venta> getVentaById(@PathVariable("idVenta") int id) throws DesarmaduriaException {
		Venta venta = ventaService.findVentaById(id);
		if (venta == null) {
			return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Venta>(venta, HttpStatus.OK);
		}
	}
	
	
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Venta> addVenta(@RequestBody Venta venta) {
		try {
			Vendedor vendedor=vendedorService.findVendedorById(venta.getVendedor().getId());
			
			Cliente cliente=clienteService.findClienteById(venta.getCliente().getId());
			
			
			ArrayList<Articulo>articulos=(ArrayList<Articulo>) articuloService.findAllArticulos();
			
			ArrayList<Articulo> nuevo=new ArrayList<Articulo>();
			
			for(int i=0;i<venta.getArticulos().size();i++) {
				for(int j=0;j<articulos.size();j++) {
					if(venta.getArticulos().get(i).getId()==articulos.get(j).getId()) {
						nuevo.add(articulos.get(j));
					}
				}
			}
			
			venta.setVendedor(vendedor);
			venta.setCliente(cliente);
			venta.setArticulos(articulos);
			
			ventaService.save(venta);
			return new ResponseEntity<Venta>(venta, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Venta>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idVenta}")
	public ResponseEntity<Venta> updateVenta(@PathVariable("idVenta") int idVenta, @RequestBody Venta venta) throws DesarmaduriaException{
			
			Venta venta1 = ventaService.findVentaById(idVenta);
			if (venta1 == null) {
				return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
			}
			Vendedor vendedor=vendedorService.findVendedorById(venta.getVendedor().getId());
			venta1.setVendedor(vendedor);
			
			Cliente cliente=clienteService.findClienteById(venta.getCliente().getId());
			venta1.setCliente(cliente);
			
			
			ArrayList<Articulo>articulos=(ArrayList<Articulo>) articuloService.findAllArticulos();
			
			ArrayList<Articulo>nuevo=new ArrayList<Articulo>();

			for(int i=0;i<venta.getArticulos().size();i++) {
				for(int j=0;j<articulos.size();j++) {
					if(venta.getArticulos().get(i).getId()==articulos.get(j).getId()) {
						nuevo.add(articulos.get(j));
					}
				}
			}
			venta1.setArticulos(nuevo);
			
			
			
			venta1.setCodigoVenta(venta.getCodigoVenta());
			venta1.setFecha(venta.getFecha());
			venta1.setDetalleVenta(venta.getDetalleVenta());
			ventaService.save(venta1);
			
			return new ResponseEntity<Venta>(venta1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idVenta}", produces="application/json")
	public ResponseEntity<Venta> deleteVentaById(@PathVariable("idVenta") int idVenta) {
		try {
			ventaService.deleteVentaById(idVenta);
			return new ResponseEntity<Venta>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Venta>(HttpStatus.NOT_FOUND);
		}
	}
}
