package com.desarmaduria.springboot.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name="mecanico")
public class Mecanico {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nombre;
	private String apellido;
	private int telefono;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id_Vendedor3")
	@NotNull
	private Vendedor vendedor;
	
	@ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name = "mecanico_vehiculo", joinColumns = @JoinColumn(name = "mecanico_id"),
    inverseJoinColumns = @JoinColumn(name = "vehiculo_id"))
	 @JsonIgnoreProperties(value="mecanicos")
    List<Vehiculo> vehiculos;
	
	
	public Mecanico() {}

	public Mecanico(String nombre, String apellido, int telefono,Vendedor vendedor, List<Vehiculo> vehiculos) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.vendedor=vendedor;
		this.vehiculos=vehiculos;
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public List<Vehiculo> getVehiculos() {
		if(vehiculos==null) {
			return new ArrayList<Vehiculo>();
		}
		return vehiculos;
	}

	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}
	
	
}
