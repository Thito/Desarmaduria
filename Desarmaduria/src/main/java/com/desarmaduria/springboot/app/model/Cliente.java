package com.desarmaduria.springboot.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
 
	
	@JoinColumn(name="rut_Cliente")
	private int rutCliente;
	
	private String nombre;
	
	private String apellido;
	
	
	public Cliente(){}
	
	public Cliente(int rutCliente, String nombre, String apellido) {
		this.rutCliente = rutCliente;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}
	
	 @JsonIgnore
	 public boolean isNew() {
	    return this.id == null;
	  }

	
	public int getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(int rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
}
