package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Mecanico;

public interface MecanicoService {

	List<Mecanico> findAllMecanicos();

	Mecanico findMecanicoById(int idMecanico) throws DesarmaduriaException;

	void save(Mecanico mecanico) throws DesarmaduriaException;

	void deleteMecanicoById(int idMecanico) throws DesarmaduriaException;

}
