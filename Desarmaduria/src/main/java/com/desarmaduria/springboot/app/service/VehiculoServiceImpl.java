package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Vehiculo;
import com.desarmaduria.springboot.app.repository.VehiculoRepository;

@Service
public class VehiculoServiceImpl implements VehiculoService {

	@Autowired
	private VehiculoRepository vehiculoRepository;

	@Override
	public List<Vehiculo> findAllVehiculo() {
		return vehiculoRepository.findAll();
	}

	@Override
	public Vehiculo findVehiculoById(int idVehiculo) throws DesarmaduriaException{
		Vehiculo vehiculo = vehiculoRepository.findById(idVehiculo);
		if (vehiculo != null) {
			return vehiculo;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Vehiculo vehiculo) throws DesarmaduriaException{
		if (vehiculo == null) {
			throw new DesarmaduriaException();
		}
		try {
			vehiculoRepository.save(vehiculo);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteVehiculoById(int idVehiculo) throws DesarmaduriaException {
		try {
			vehiculoRepository.deleteById(idVehiculo);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
