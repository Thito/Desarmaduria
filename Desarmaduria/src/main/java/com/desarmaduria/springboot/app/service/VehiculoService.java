package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Vehiculo;

public interface VehiculoService {

	List<Vehiculo> findAllVehiculo();

	Vehiculo findVehiculoById(int idVehiculo) throws DesarmaduriaException;

	void save(Vehiculo vehiculo) throws DesarmaduriaException;

	void deleteVehiculoById(int idVehiculo) throws DesarmaduriaException;

}
