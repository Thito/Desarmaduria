package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Cliente;
import com.desarmaduria.springboot.app.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<Cliente> findAllClientes() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente findClienteById(int idCliente) throws DesarmaduriaException {
		Cliente cliente = clienteRepository.findById(idCliente);
		if (cliente != null) {
			return cliente;
		} else {
			throw new DesarmaduriaException();
		}
	}

	@Transactional
	@Override
	public Cliente save(Cliente cliente) {

		if (cliente == null) {
			return null;
		}

		return clienteRepository.save(cliente);

	}

	@Transactional
	@Override
	public void deleteClienteById(int idCliente) throws DesarmaduriaException {
		try {
			clienteRepository.deleteById(idCliente);
		} catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}

	}

}
