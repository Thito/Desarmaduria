package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Articulo;

public interface ArticuloService {
	

	List<Articulo> findAllArticulos() ;
	
	int getStockByNombreArticulo(String nombre);

	Articulo findArticuloById(int idArticulo) throws DesarmaduriaException;

	void save(Articulo articulo) throws DesarmaduriaException;

	void deleteArticuloById(int idArticulo) throws DesarmaduriaException;

	List<Articulo> getArticulosPorBodega(String nomBodega);

	


}
