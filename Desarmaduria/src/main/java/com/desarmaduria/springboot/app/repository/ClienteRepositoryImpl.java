package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Cliente;


@Repository
@Profile("jpa")
public class ClienteRepositoryImpl implements ClienteRepository {
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")

	@Override
	public List<Cliente> findAll()  {
		// TODO Auto-generated method stub
		return this.em.createQuery("SELECT cliente FROM Cliente cliente").getResultList();
	}

	@Override
	public Cliente findById(int id)  {
		// TODO Auto-generated method stub
		return this.em.find(Cliente.class, id);
	}

	@Override
	public Cliente save(Cliente cliente)  {
		// TODO Auto-generated method stub
		   if (cliente.getId() == null) {
	            this.em.persist(cliente);
	            
	        } else {
	            this.em.merge(cliente);
	        }
		   
		   return cliente;
		
	}

	@Override
	public void deleteById(int id)  {
		// TODO Auto-generated method stub
		Cliente cliente = this.findById(id);
		this.em.remove(cliente);
		
	}
	

}
