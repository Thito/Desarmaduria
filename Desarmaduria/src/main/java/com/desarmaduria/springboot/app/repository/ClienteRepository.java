package com.desarmaduria.springboot.app.repository;

import java.util.List;

import org.springframework.dao.DataAccessException;

import  com.desarmaduria.springboot.app.model.Cliente;

public interface ClienteRepository {
	
	public List<Cliente> findAll() throws DataAccessException;
	
	public Cliente findById(int id) throws DataAccessException;

	public Cliente save(Cliente cliente) throws DataAccessException;

	public void deleteById(int id) throws DataAccessException;
	

}
