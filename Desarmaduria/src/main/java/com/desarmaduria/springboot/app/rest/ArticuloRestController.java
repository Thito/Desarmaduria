package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Articulo;
import com.desarmaduria.springboot.app.service.ArticuloService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;

@RestController
@RequestMapping("articulos")
public class ArticuloRestController {
	
	
	@Autowired
	private ArticuloService articuloService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Articulo>> getAllArticulos() {
		List<Articulo> lista = articuloService.findAllArticulos();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Articulo>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Articulo>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idArticulo}", produces="application/json")
	public ResponseEntity<Articulo> getArticuloById(@PathVariable("idArticulo") int id) throws DesarmaduriaException { 
		Articulo articulo = articuloService.findArticuloById(id);
		if (articulo == null) {
			return new ResponseEntity<Articulo>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Articulo>(articulo, HttpStatus.OK);
		}
	}
	
	//LISTAR ARTICULOS DE UNA BODEGA
	@GetMapping(value = "/articulosEnBodega/{nombreBodega}", produces = "application/json")
	public ResponseEntity<List<Articulo>> getArticulosByNombreBodega(@PathVariable("nombreBodega") String ubicacion) {
		List<Articulo> lista = articuloService.getArticulosPorBodega(ubicacion);
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Articulo>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Articulo>>(lista,HttpStatus.OK);
		}
	}
	
	//OBTENER EL LUGAR DONDE SE ALMACENA EL ARTICULO
	@GetMapping(value="/ubicacion/{idArticulo}", produces="application/json")
	public ResponseEntity<String> getUbicacionArticuloById(@PathVariable("idArticulo") int id) throws DesarmaduriaException { 
		Articulo articulo = articuloService.findArticuloById(id);
		if (articulo == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<String>(articulo.getUbicacion(), HttpStatus.OK);
		}
	}
	
	//OBTIENE EL STOCK DE UN ARTICULO
	@GetMapping(value="/stock/{nombreArticulo}", produces="application/json")
	public ResponseEntity<Integer> getStockArticuloByNombre(@PathVariable("nombreArticulo") String nombre) { 
		int stock = articuloService.getStockByNombreArticulo(nombre);
		if (stock == 0) {
			return new ResponseEntity<Integer>(0,HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Integer>(stock, HttpStatus.OK);
		}
	}
	//Servicio obtener el precio de un articulo en especifico
		@GetMapping(value="/precio/{idArticulo}", produces="application/json")
		public ResponseEntity<Integer> getPrecioArticuloById(@PathVariable("idArticulo") int id) throws DesarmaduriaException { 
			Articulo articulo = articuloService.findArticuloById(id);
			if (articulo == null) {
				return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
			} else {
				return new ResponseEntity<Integer>(articulo.getPrecio(), HttpStatus.OK);
			}
		}
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Articulo> addArticulo(@RequestBody Articulo articulo) { 
		try {
			articuloService.save(articulo);
			return new ResponseEntity<Articulo>(articulo, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Articulo>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idArticulo}")
	public ResponseEntity<Articulo> updateArticulo(@PathVariable("idArticulo") int idArticulo, @RequestBody Articulo articulo) throws DesarmaduriaException{
			Articulo articulo1 = articuloService.findArticuloById(idArticulo);
			if (articulo1 == null) {
				return new ResponseEntity<Articulo>(HttpStatus.NOT_FOUND);
			}
			articulo1.setNombre(articulo.getNombre());
			articulo1.setUbicacion(articulo.getUbicacion());
			articulo1.setPrecio(articulo.getPrecio());
		

			articuloService.save(articulo1);
			return new ResponseEntity<Articulo>(articulo1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idArticulo}", produces="application/json")
	public ResponseEntity<Articulo> deleteArticuloById(@PathVariable("idArticulo") int idArticulo) {
		try {
			articuloService.deleteArticuloById(idArticulo);
			return new ResponseEntity<Articulo>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Articulo>(HttpStatus.NOT_FOUND);
		}
	}
	


}
