package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Articulo;

@Repository
@Profile("jpa")
public class ArticuloRepositoryImpl implements ArticuloRepository {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")

	@Override
	public List<Articulo> findAll() {
		return this.em.createQuery("SELECT articulo FROM Articulo articulo").getResultList();
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public List<Articulo> articulosByBodega(String nombre) {
	 * 
	 * List<Articulo> lista; List<Articulo>listaAux=new ArrayList<Articulo>();
	 * 
	 * 
	 * 
	 * lista=
	 * this.em.createQuery("SELECT articulo FROM Articulo articulo").getResultList()
	 * ;
	 * 
	 * for(int i=0;i<lista.size();i++) {
	 * if(lista.get(i).getUbicacion().equals(nombre)) { listaAux.add(lista.get(i));
	 * } } return listaAux; }
	 */

	@Override
	public Articulo findById(int id) {
		// TODO Auto-generated method stub
		return this.em.find(Articulo.class, id);

	}

	@Override
	public void save(Articulo articulo) {
		// TODO Auto-generated method stub

		if (articulo.getId() == null) {
			this.em.persist(articulo);
		} else {
			this.em.merge(articulo);
		}

	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		Articulo articulo = this.findById(id);
		this.em.remove(articulo);

	}

}
