package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Garage;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.GarageService;

@RestController
@RequestMapping("garages")
public class GarageRestController {

	@Autowired
	private GarageService garageService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Garage>> getAllGarages() {
		List<Garage> lista = garageService.findAllGarages();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Garage>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Garage>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idGarage}", produces="application/json")
	public ResponseEntity<Garage> getGarageById(@PathVariable("idGarage") int id) throws DesarmaduriaException {
		Garage garage = garageService.findGarageById(id);
		if (garage == null) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Garage>(garage, HttpStatus.OK);
		}
	}
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Garage> addGarage(@RequestBody Garage garage) {
		try {
			garageService.save(garage);
			return new ResponseEntity<Garage>(garage, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Garage>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idGarage}")
	public ResponseEntity<Garage> updateGarage(@PathVariable("idGarage") int idGarage, @RequestBody Garage garage) throws DesarmaduriaException{
			
			Garage garage1 = garageService.findGarageById(idGarage);
			if (garage1 == null) {
				return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
			}
			
			garage1.setCapacidad(garage.getCapacidad());
			garageService.save(garage1);
			
			return new ResponseEntity<Garage>(garage1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idGarage}", produces="application/json")
	public ResponseEntity<Garage> deleteGarageById(@PathVariable("idGarage") int idGarage) {
		try {
			garageService.deleteGarageById(idGarage);
			return new ResponseEntity<Garage>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Garage>(HttpStatus.NOT_FOUND);
		}
	}
}
