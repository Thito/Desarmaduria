package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Vehiculo;

public interface VehiculoRepository {

	List<Vehiculo> findAll();

	Vehiculo findById(int id);

	void save(Vehiculo vehiculo);

	void deleteById(int id);

}
