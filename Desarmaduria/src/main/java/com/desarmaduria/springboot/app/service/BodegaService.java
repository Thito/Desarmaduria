package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Bodega;

public interface BodegaService {

	List<Bodega> findAllBodegas();

	Bodega findBodegaById(int idBodega) throws DesarmaduriaException;

	void save(Bodega bodega) throws DesarmaduriaException;

	void deleteBodegaById(int idBodega) throws DesarmaduriaException;

}
