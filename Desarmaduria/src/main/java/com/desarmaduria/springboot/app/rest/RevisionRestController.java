package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Cliente;
import com.desarmaduria.springboot.app.model.Revision;
import com.desarmaduria.springboot.app.model.Vendedor;
import com.desarmaduria.springboot.app.service.ClienteService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.RevisionService;
import com.desarmaduria.springboot.app.service.VendedorService;

@RestController
@RequestMapping("revisiones")
public class RevisionRestController {

	@Autowired
	private RevisionService revisionService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private VendedorService vendedorService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Revision>> getAllRevisiones() {
		List<Revision> lista = revisionService.findAllRevisiones();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Revision>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Revision>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idRevision}", produces="application/json")
	public ResponseEntity<Revision> getRevisionById(@PathVariable("idRevision") int id) throws DesarmaduriaException {
		Revision revision = revisionService.findRevisionById(id);
		if (revision == null) {
			return new ResponseEntity<Revision>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Revision>(revision, HttpStatus.OK);
		}
	}
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Revision> addRevision(@RequestBody Revision revision) {
		try {
			Cliente cliente=clienteService.findClienteById(revision.getCliente().getId());
			Vendedor vendedor=vendedorService.findVendedorById(revision.getVendedor().getId());
			
			revision.setCliente(cliente);
			revision.setVendedor(vendedor);
			revisionService.save(revision);
			return new ResponseEntity<Revision>(revision, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Revision>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idRevision}")
	public ResponseEntity<Revision> updateRevision(@PathVariable("idRevision") int idRevision, @RequestBody Revision revision) throws DesarmaduriaException{
			
			Revision revision1 = revisionService.findRevisionById(idRevision);
			if (revision1 == null) {
				return new ResponseEntity<Revision>(HttpStatus.NOT_FOUND);
			}
			
			Cliente cliente=clienteService.findClienteById(revision.getCliente().getId());
			Vendedor vendedor=vendedorService.findVendedorById(revision.getVendedor().getId());
			
			revision1.setFecha(revision.getFecha());
			revision1.setDetalleRevision(revision.getDetalleRevision());
			revision1.setCliente(cliente);
			revision1.setVendedor(vendedor);
			
			revisionService.save(revision1);
			
			return new ResponseEntity<Revision>(revision1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idRevision}", produces="application/json")
	public ResponseEntity<Revision> deleteVentaById(@PathVariable("idRevision") int idRevision) {
		try {
			revisionService.deleteRevisionById(idRevision);
			return new ResponseEntity<Revision>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Revision>(HttpStatus.NOT_FOUND);
		}
	}
	
}
