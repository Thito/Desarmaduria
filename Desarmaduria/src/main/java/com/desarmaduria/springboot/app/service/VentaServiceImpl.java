package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Venta;
import com.desarmaduria.springboot.app.repository.VentaRepository;

@Service
public class VentaServiceImpl implements VentaService {

	@Autowired
	private VentaRepository ventaRepository;
	
	@Override
	public List<Venta> findAllVentas() {
		return ventaRepository.findAll();
	}

	@Override
	public Venta findVentaById(int idVenta) throws DesarmaduriaException{
		Venta venta = ventaRepository.findById(idVenta);
		if (venta != null) {
			return venta;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Venta venta) throws DesarmaduriaException{
		if (venta == null) {
			throw new DesarmaduriaException();
		}
		try {
			ventaRepository.save(venta);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteVentaById(int idVenta) throws DesarmaduriaException {
		try {
			ventaRepository.deleteById(idVenta);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
