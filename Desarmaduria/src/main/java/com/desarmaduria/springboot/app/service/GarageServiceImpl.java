package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Garage;
import com.desarmaduria.springboot.app.repository.GarageRepository;

@Service
public class GarageServiceImpl implements GarageService {

	@Autowired
	private GarageRepository garageRepository;

	@Override
	public List<Garage> findAllGarages() {
		return garageRepository.findAll();
	}

	@Override
	public Garage findGarageById(int idGarage) throws DesarmaduriaException{
		Garage garage = garageRepository.findById(idGarage);
		if (garage != null) {
			return garage;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Garage garage) throws DesarmaduriaException{
		if (garage == null) {
			throw new DesarmaduriaException();
		}
		try {
			garageRepository.save(garage);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteGarageById(int idGarage) throws DesarmaduriaException {
		try {
			garageRepository.deleteById(idGarage);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
