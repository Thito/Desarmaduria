package com.desarmaduria.springboot.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.desarmaduria.springboot.app.model.Revision;
import com.desarmaduria.springboot.app.repository.RevisionRepository;

@Service
public class RevisionServiceImpl implements RevisionService {

	@Autowired
	private RevisionRepository revisionRepository;

	@Override
	public List<Revision> findAllRevisiones() {
		return revisionRepository.findAll();
	}

	@Override
	public Revision findRevisionById(int idRevision) throws DesarmaduriaException{
		Revision revision = revisionRepository.findById(idRevision);
		if (revision != null) {
			return revision;
		} else {
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void save(Revision revision) throws DesarmaduriaException{
		if (revision == null) {
			throw new DesarmaduriaException();
		}
		try {
			revisionRepository.save(revision);
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new DesarmaduriaException();
		}
	}
	
	@Transactional
	@Override
	public void deleteRevisionById(int idRevision) throws DesarmaduriaException {
		try {
			revisionRepository.deleteById(idRevision);	
		}
		catch (DataAccessException e) {
			throw new DesarmaduriaException();
		}		
		
	}
}
