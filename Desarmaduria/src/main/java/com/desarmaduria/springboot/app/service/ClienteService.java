package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Cliente;

public interface ClienteService {

	List<Cliente> findAllClientes();

	Cliente findClienteById(int idCliente) throws DesarmaduriaException;

	Cliente save(Cliente cliente) ;

	void deleteClienteById(int idCliente) throws DesarmaduriaException;

}
