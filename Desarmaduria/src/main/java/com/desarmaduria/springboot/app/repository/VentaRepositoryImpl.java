package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Venta;

@Repository
@Profile("jpa")
public class VentaRepositoryImpl implements VentaRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Venta> findAll() {
		return this.em.createQuery("SELECT venta FROM Venta venta").getResultList();
	}
	
	@Override
	public Venta findById(int id) {
		return this.em.find(Venta.class, id);
	}
	
	@Override
	public void save(Venta venta)  {
        if (venta.getId() == null) {
            this.em.persist(venta);
        } else {
            this.em.merge(venta);
        }
	}

	@Override
	public void deleteById(int id) {
		Venta venta = this.findById(id);
		this.em.remove(venta);
	}	
}
