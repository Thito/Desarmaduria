package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Cliente;

import com.desarmaduria.springboot.app.service.ClienteService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;

@RestController
@RequestMapping("clientes")
public class ClienteRestController {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Cliente>> getAllClientes() {
		List<Cliente> lista = clienteService.findAllClientes();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Cliente>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Cliente>>(lista,HttpStatus.OK);
		}
	}
	
	//SERVICIO OBTENER LOS DATOS POR CLIENTE DADO UN IDCLIENTE
	@GetMapping(value="/{idCliente}", produces="application/json")
	public ResponseEntity<Cliente> obtenerDatosClienteById(@PathVariable("idCliente") int id) throws DesarmaduriaException { 
		Cliente cliente = clienteService.findClienteById(id);
		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
		}
	}
	
	//SERVICIO AÑADIR CLIENTES
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Cliente> anadirClientes(@RequestBody Cliente cliente) { 
		if(cliente!=null) {
			Cliente cliente2=clienteService.save(cliente);
			return new ResponseEntity<Cliente>(cliente2, HttpStatus.CREATED);
		} else  {
			return new ResponseEntity<Cliente>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idCliente}")
	public ResponseEntity<Cliente> updateCliente(@PathVariable("idCliente") int idCliente, @RequestBody Cliente cliente) throws DesarmaduriaException{
			Cliente cliente1 = clienteService.findClienteById(idCliente);
			if (cliente1 == null) {
				return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
			}
			cliente1.setRutCliente(cliente.getRutCliente());
			cliente1.setNombre(cliente.getNombre());
			cliente1.setApellido(cliente.getApellido());
			clienteService.save(cliente1);
			return new ResponseEntity<Cliente>(cliente1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idCliente}", produces="application/json")
	public ResponseEntity<Cliente> deleteClienteById(@PathVariable("idCliente") int idCliente) {
		try {
			clienteService.deleteClienteById(idCliente);
			return new ResponseEntity<Cliente>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
	}
	
	

}
