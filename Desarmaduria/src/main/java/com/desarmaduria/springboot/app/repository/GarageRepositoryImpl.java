package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Garage;

@Repository
@Profile("jpa")
public class GarageRepositoryImpl implements GarageRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Garage> findAll() {
		return this.em.createQuery("SELECT garage FROM Garage garage").getResultList();
	}
	
	@Override
	public Garage findById(int id) {
		return this.em.find(Garage.class, id);
	}
	
	@Override
	public void save(Garage garage)  {
        if (garage.getId() == null) {
            this.em.persist(garage);
        } else {
            this.em.merge(garage);
        }
	}

	@Override
	public void deleteById(int id) {
		Garage garage = this.findById(id);
		this.em.remove(garage);
	}
}
