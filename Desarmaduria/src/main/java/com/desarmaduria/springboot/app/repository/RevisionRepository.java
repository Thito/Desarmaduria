package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Revision;

public interface RevisionRepository {

	List<Revision> findAll();

	Revision findById(int id);

	void save(Revision revision);

	void deleteById(int id);

}
