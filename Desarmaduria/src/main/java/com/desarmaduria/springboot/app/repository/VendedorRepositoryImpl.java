package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Vendedor;

@Repository
@Profile("jpa")
public class VendedorRepositoryImpl implements VendedorRepository {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Vendedor> findAll() {
		return this.em.createQuery("SELECT vendedor FROM Vendedor vendedor").getResultList();
	}
	
	@Override
	public Vendedor findById(int id) {
		return this.em.find(Vendedor.class, id);
	}
	
	@Override
	public void save(Vendedor vendedor)  {
        if (vendedor.getId() == null) {
            this.em.persist(vendedor);
        } else {
            this.em.merge(vendedor);
        }
	}
	
	@Override
	public void deleteById(int id) {
		Vendedor vendedor = this.findById(id);
		this.em.remove(vendedor);
	}
}
