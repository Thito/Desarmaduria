package com.desarmaduria.springboot.app.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Cliente;
import com.desarmaduria.springboot.app.model.Garage;
import com.desarmaduria.springboot.app.model.Mecanico;
import com.desarmaduria.springboot.app.model.Vehiculo;
import com.desarmaduria.springboot.app.service.ClienteService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;
import com.desarmaduria.springboot.app.service.GarageService;
import com.desarmaduria.springboot.app.service.MecanicoService;
import com.desarmaduria.springboot.app.service.VehiculoService;

@RestController
@RequestMapping("vehiculos")
public class VehiculoRestController {

	@Autowired
	private VehiculoService vehiculoService;
	@Autowired
	private ClienteService clienteService;
	@Autowired 
	private MecanicoService mecanicoService;
	@Autowired
	private GarageService garageService;
	
	
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Vehiculo>> getAllVehiculo() {
		List<Vehiculo> lista = vehiculoService.findAllVehiculo();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Vehiculo>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Vehiculo>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idVehiculo}", produces="application/json")
	public ResponseEntity<Vehiculo> getVehiculoById(@PathVariable("idVehiculo") int id) throws DesarmaduriaException {
		Vehiculo vehiculo = vehiculoService.findVehiculoById(id);
		if (vehiculo == null) {
			return new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Vehiculo>(vehiculo, HttpStatus.OK);
		}
	}
	
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Vehiculo> addVehiculo(@RequestBody Vehiculo vehiculo) {
		try {
			Cliente cliente=clienteService.findClienteById(vehiculo.getCliente().getId());
			
			
			
			
			ArrayList<Mecanico>mecanicos=(ArrayList<Mecanico>) mecanicoService.findAllMecanicos();
			
			ArrayList<Mecanico>nuevo=new ArrayList<Mecanico>();
			
			for(int i=0;i<vehiculo.getMecanicos().size();i++) {
				for(int j=0;j<mecanicos.size();j++) {
					if(vehiculo.getMecanicos().get(i).getId()==mecanicos.get(j).getId()) {
						nuevo.add(mecanicos.get(j));
					}
				}
			}
			
			Garage garage=garageService.findGarageById(vehiculo.getGarage().getId());
			
			
			vehiculo.setCliente(cliente);
			vehiculo.setMecanicos(nuevo);
			vehiculo.setGarage(garage);
			
			vehiculoService.save(vehiculo);
			return new ResponseEntity<Vehiculo>(vehiculo, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Vehiculo>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idVehiculo}")
	public ResponseEntity<Vehiculo> updateVehiculo(@PathVariable("idVehiculo") int idVehiculo, @RequestBody Vehiculo vehiculo) throws DesarmaduriaException{
			
		Vehiculo vehiculo1 = vehiculoService.findVehiculoById(idVehiculo);
			if (vehiculo1 == null) {
				return new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
			}
			Cliente cliente=clienteService.findClienteById(vehiculo.getCliente().getId());
			
			vehiculo1.setCliente(cliente);
			

			ArrayList<Mecanico>mecanicos=(ArrayList<Mecanico>) mecanicoService.findAllMecanicos();
			
			ArrayList<Mecanico>nuevo=new ArrayList<Mecanico>();
			
			for(int i=0;i<vehiculo.getMecanicos().size();i++) {
				for(int j=0;j<mecanicos.size();j++) {
					if(vehiculo.getMecanicos().get(i).getId()==mecanicos.get(j).getId()) {
						nuevo.add(mecanicos.get(j));
					}
				}
			}
			vehiculo1.setMecanicos(nuevo);
			
			Garage garage=garageService.findGarageById(vehiculo.getGarage().getId());
			
			vehiculo1.setGarage(garage);
			
			
			
		
			
			
	
			
			
			vehiculo1.setMarca(vehiculo.getMarca());
			vehiculo1.setModelo(vehiculo.getModelo());
			vehiculo1.setAño(vehiculo.getAño());
			
			
			vehiculoService.save(vehiculo1);
			
			return new ResponseEntity<Vehiculo>(vehiculo1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idVehiculo}", produces="application/json")
	public ResponseEntity<Vehiculo> deleteVehiculoById(@PathVariable("idVehiculo") int idVehiculo) {
		try {
			vehiculoService.deleteVehiculoById(idVehiculo);
			return new ResponseEntity<Vehiculo>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
		}
	}
}
