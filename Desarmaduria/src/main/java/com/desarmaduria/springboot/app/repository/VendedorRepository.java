package com.desarmaduria.springboot.app.repository;

import java.util.List;


import org.springframework.dao.DataAccessException;

import com.desarmaduria.springboot.app.model.Vendedor;


public interface VendedorRepository {

	public List<Vendedor> findAll() throws DataAccessException;

	
	//public List<Cliente> findAll() throws DataAccessException;
	public Vendedor findById(int id) throws DataAccessException;

	public void save(Vendedor vendedor) throws DataAccessException;

	public void deleteById(int id) throws DataAccessException;
}
