package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Bodega;


@Repository
@Profile("jpa")
public class BodegaRepositoryImpl implements BodegaRepository {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Bodega> findAll() {
		return this.em.createQuery("SELECT bodega FROM Bodega bodega").getResultList();
	}
	
	@Override
	public Bodega findById(int id) {
		return this.em.find(Bodega.class, id);
	}
	
	@Override
	public void save(Bodega bodega)  {
        if (bodega.getId() == null) {
            this.em.persist(bodega);
        } else {
            this.em.merge(bodega);
        }
	}

	@Override
	public void deleteById(int id) {
		Bodega bodega = this.findById(id);
		this.em.remove(bodega);
	}	
}
