package com.desarmaduria.springboot.app.repository;

import java.util.List;

import com.desarmaduria.springboot.app.model.Garage;

public interface GarageRepository {

	List<Garage> findAll();

	Garage findById(int id);

	void save(Garage garage);

	void deleteById(int id);

}
