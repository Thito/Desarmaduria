package com.desarmaduria.springboot.app.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.desarmaduria.springboot.app.model.Revision;

@Repository
@Profile("jpa")
public class RevisionRepositoryImpl implements RevisionRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Revision> findAll() {
		return this.em.createQuery("SELECT revision FROM Revision revision").getResultList();
	}
	
	@Override
	public Revision findById(int id) {
		return this.em.find(Revision.class, id);
	}
	
	@Override
	public void save(Revision revision)  {
        if (revision.getId() == null) {
            this.em.persist(revision);
        } else {
            this.em.merge(revision);
        }
	}

	@Override
	public void deleteById(int id) {
		Revision revision = this.findById(id);
		this.em.remove(revision);
	}
}
