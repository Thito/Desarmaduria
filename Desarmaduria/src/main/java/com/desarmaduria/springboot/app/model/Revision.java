package com.desarmaduria.springboot.app.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="revision")
public class Revision {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	
	private Date fecha;
	
	private String detalleRevision;
	
	@OneToOne
	@JoinColumn(name = "id_Cliente2")
	private Cliente cliente;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "id_Vendedor2")
	private Vendedor vendedor;
	
	public Revision() {}

	public Revision(Date fecha, String detalleRevision, Cliente cliente, Vendedor vendedor) {
		this.fecha = fecha;
		this.detalleRevision = detalleRevision;
		this.cliente = cliente;
		this.vendedor = vendedor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	 @JsonIgnore
	 public boolean isNew() {
	    return this.id == null;
	  }

	public Date getFecha() {
		return fecha;
	}
	
	
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDetalleRevision() {
		return detalleRevision;
	}

	public void setDetalleRevision(String detalleRevision) {
		this.detalleRevision = detalleRevision;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		
		this.cliente = cliente;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}
	
	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}
	
	
	

}
