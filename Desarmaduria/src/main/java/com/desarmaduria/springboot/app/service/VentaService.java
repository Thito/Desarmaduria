package com.desarmaduria.springboot.app.service;

import java.util.List;

import com.desarmaduria.springboot.app.model.Venta;

public interface VentaService {

	List<Venta> findAllVentas();

	Venta findVentaById(int idVenta) throws DesarmaduriaException;

	void save(Venta venta) throws DesarmaduriaException;

	void deleteVentaById(int idVenta) throws DesarmaduriaException;

}
