package com.desarmaduria.springboot.app.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desarmaduria.springboot.app.model.Bodega;
import com.desarmaduria.springboot.app.service.BodegaService;
import com.desarmaduria.springboot.app.service.DesarmaduriaException;

@RestController
@RequestMapping("bodegas")
public class BodegaRestController {

	@Autowired
	private BodegaService bodegaService;
	
	@GetMapping(value = "", produces = "application/json")
	public ResponseEntity<List<Bodega>> getAllBodega() {
		List<Bodega> lista = bodegaService.findAllBodegas();
		if (lista.isEmpty()) {
			return new ResponseEntity<List<Bodega>>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Bodega>>(lista,HttpStatus.OK);
		}
	}
	
	@GetMapping(value="/{idBodega}", produces="application/json")
	public ResponseEntity<Bodega> getBodegaById(@PathVariable("idBodega") int id) throws DesarmaduriaException {
		Bodega bodega = bodegaService.findBodegaById(id);
		if (bodega == null) {
			return new ResponseEntity<Bodega>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Bodega>(bodega, HttpStatus.OK);
		}
	}
	//Servicio obtener capacidad total de una bodega
	@GetMapping(value="/capacidad/{idBodega}", produces="application/json")
	public ResponseEntity<Integer> getCapacidadTotalBodegaById(@PathVariable("idBodega") int id) throws DesarmaduriaException {
			Bodega bodega = bodegaService.findBodegaById(id);
			if (bodega == null) {
				return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
			} else {
				return new ResponseEntity<Integer>(bodega.getCapacidadDeAlmacenamiento(), HttpStatus.OK);
			}
		}
	@PostMapping(value = "", produces = "application/json")
	public ResponseEntity<Bodega> addBodega(@RequestBody Bodega bodega) {
		try {
			bodegaService.save(bodega);
			return new ResponseEntity<Bodega>(bodega, HttpStatus.CREATED);
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Bodega>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{idBodega}")
	public ResponseEntity<Bodega> updateBodega(@PathVariable("idBodega") int idBodega, @RequestBody Bodega bodega) throws DesarmaduriaException{
			
			Bodega bodega1 = bodegaService.findBodegaById(idBodega);
			if (bodega1 == null) {
				return new ResponseEntity<Bodega>(HttpStatus.NOT_FOUND);
			}
			
			bodega1.setNombre(bodega1.getNombre());
			bodega1.setCapacidadDeAlmacenamiento(bodega1.getCapacidadDeAlmacenamiento());
			bodega1.setArticulos(bodega1.getArticulos());
			
			return new ResponseEntity<Bodega>(bodega1, HttpStatus.NO_CONTENT);
	}
	
	@DeleteMapping(value="/{idBodega}", produces="application/json")
	public ResponseEntity<Bodega> deleteVentaById(@PathVariable("idBodega") int idBodega) {
		try {
			bodegaService.deleteBodegaById(idBodega);
			return new ResponseEntity<Bodega>(HttpStatus.OK);	
		} catch (DesarmaduriaException e) {
			return new ResponseEntity<Bodega>(HttpStatus.NOT_FOUND);
		}
	}
}
